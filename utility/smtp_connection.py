"""
  SmtpConnection
  v 0.01
 
  Wrap the smtp library interface into a modular object. 
  Hide the details to provide a simplisitc interface for sending emails
  from other workflows. 

  The methods are divided to lend themselfs better to unit testing.  
"""

# import smtpLib to wrap the socket api
import smtplib
import logging

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

class smtp_connection:

    def __init__(self, 
                 username,
                 password,
                 fromAddress, 
                 toAddress,
                 smtpHost, 
                 smtpPort,  
                 bodyContentType,
                 bodyContent, 
                 header="",
                 pathToAttachment="",
                 ccAddress=[]):

        # perform our parameter checking
        if fromAddress == "":
            raise ValueError("Parameter: 'fromAddress' cannot be null")
        if len(toAddress) == 0:
            raise ValueError("Parameter: 'toAddress' cannot be null")
        if smtpHost == "":
            raise ValueError("Parameter: 'smtpHost' cannot be null")
        if header == "":
            logging.warning("Parameter:  'header' was passed as null")
        
        self.username    = username
        self.password    = password
        self.fromAddress = fromAddress
        self.toAddress = toAddress
        self.ccAddress = ccAddress
        self.smtpHost = smtpHost
        self.smtpPort = smtpPort
        self.header = header
        self.bodyContent = bodyContent
        self.bodyContentType = bodyContentType
        self.pathToAttachment = pathToAttachment

        # our object to encapsulate the uderlying socket connection
        # and expose utility methods sendMessage(), and quit()
        self.smtpServer = None

        # A MIME descriptor for capturing message payload type
        # text/plain, text/html
        self.mimeMsg = None


    def establishConnection(self):
        # try to establish a connection to the remote smtp host. Only connection exceptions are caught
        # but a whole slew of other exceptions exist for this particular call to catch on
        try:
            self.smtpServer = smtplib.SMTP(self.smtpHost, self.smtpPort)
            self.smtpServer.ehlo()
            self.smtpServer.starttls()
            self.smtpServer.ehlo
            self.smtpServer.login(self.username, self.password)
        except smtplib.SMTPConnectError as e:
            logging.error("Caught exception SMTPConnectError: " + repr(e))
            return False

        return True
    
    def sendMail(self):
        # if, for whatever reason, someone calls this method with a null smtpServer object, we should 
        # check for that
        if self.smtpServer is None: 
            logging.error("smtpServer object is null")
            return False
    
        # fill out our message headers with a MIMEMultipart object
        msg = MIMEMultipart('alternative')
        msg['Subject'] = self.header
        msg['To'] = ",".join(self.toAddress)
        msg['Cc'] = ",".join(self.ccAddress)

        # concatenate our lists into a string
        concatenatedAddresses = self.toAddress + self.ccAddress

        # create our pre-built email footer. Vary the type depending on what mime option the user 
        # chose
        if self.bodyContentType == 'html': 
            footerContent = '<p><br><br><br>If you have any questions or concerns, please contact the SIS Development Team at <a href="mailto:DevTeam@ThinkSIS.COM?Subject=response to DevTeam">DevTeam@ThinkSIS.com</a>.<br><br>Sincerely, <br><br></font><font size="5px" color="#003366" face="calibri"><b>The Development Team | <a href="mailto:DevTeam@ThinkSIS.COM?Subject=response to DevTeam">DevTeam@ThinkSIS.com</a><p>'
        else:
            footerContent = '\n\n\n If you have any questions or concerns please contact the SIS Development Team at DevTeam@thinksis.com\n Sincerely, \nThe Development Team | DevTeam@ThinkSIS.com'

        # create our mimeMsg
        self.mimeMsg = MIMEText(self.bodyContent+footerContent, self.bodyContentType)

        # add it to our MimeMultipart object message container
        msg.attach(self.mimeMsg)
        att = None

        # was there any attachments specified?
        if self.pathToAttachment != "":
            try:
                fp = open(self.pathToAttachment,'rb')
            except IOError as e:
                logging.error("Error: Could not locate attachment")
                return False

            # find out the extension of the file
            directories = self.pathToAttachment.split('/')
            fileSplit = directories[len(directories)-1].split('.')    
            if len(fileSplit) == 1:
                logging.error("PathToAttachment must contain file extension")
                logging.info("Mail will still be sent")
            else:
                # set the read pointer, and extension type as parameters to the MimeApplication constructor
                att = MIMEApplication(fp.read(),_subtype=fileSplit[1])
                fp.close()

                # add the header to the attachment object, and add that to our MIMEMultipart object
                att.add_header('Content-Disposition','attachment',filename=(fileSplit[0]+'.'+fileSplit[1]))
                msg.attach(att)
    
        self.smtpServer.sendmail(self.username, concatenatedAddresses, msg.as_string())
        return True
        
    # our destructor
    def __del__(self):
        # ensure the smtpServer object is not null, and terminate the connection
        if self.smtpServer is not None:
            self.smtpServer.close()
