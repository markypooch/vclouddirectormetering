from   pyvcloud.vcd.client              import *
from   pyvcloud.vcd.org                 import *

from   metering.vrli_logger             import *
from   metering.vcd_db                  import *
from   metering.vcd_shared_rest_methods import *

import threading, requests, base64, xmltodict, xml, datetime, cProfile, argparse, json, sys
requests.packages.urllib3.disable_warnings()

mutex = threading.Lock()
edge_details_list = []

# invoked per thread
def write_edge_uplink_statistics(vrli_log_url, 
                                 client, 
                                 org_resource, 
                                 url,
                                 db_host,
                                 db_name,
                                 db_username,
                                 db_password,
                                 organization, 
                                 username,
                                 password):
    global edge_details_list
    try:
        # Since pyVCloud _does not_ support NSX functionality, we need to tap into the 
        # underlying Rest api in order to get the interface statistics for each edge of the
        # vdc's for the each organization
        headers = ascertain_x_vcloud_token(vrli_log_url, url, username, organization, password)

        org           = Org(client, resource=org_resource)
        vdc_resources = org.list_vdcs()
    
    except Exception as exception:

        exception_payload = "\n line number {0} \n exception: ".format(sys.exc_info()[-1].tb_lineno, repr(exception))
        log_vrli(vrli_log_url, 
                "[{0}] Exception raised \nclient : {1} \n\n exception: {2}".format(org.get_name(), client, exception_payload), 
                {'appname':'prod:nsx_computeparser', 'level':'error'})
        release_x_vcloud_token(vrli_log_url, url, headers)
        return

    try:

        # for each vdc, grab it's edges
        for vdc_resource in vdc_resources:

            try:
                response      = rest_call(vrli_log_url, org.get_name(), "get", url, "/network/edges", headers)
                response_dict = xmltodict.parse(response.text)

                edge_ids, edge_list = [], []
                if not isinstance(response_dict['pagedEdgeList']['edgePage']['edgeSummary'], list):
                    edge_list = [response_dict['pagedEdgeList']['edgePage']['edgeSummary']]
                else:
                    edge_list = response_dict['pagedEdgeList']['edgePage']['edgeSummary']

                for edge in edge_list:
                    # grab the edges that belong to this vdc
                    if vdc_resource['name'] == edge['datacenterName']:
                        edge_ids.append(edge['id'])
                
                # if no edges, return
                if len(edge_ids) <= 0:
                    return
            except Exception as exception:
                exception_payload = "\n line number {0} \n exception: {1}".format(sys.exc_info()[-1].tb_lineno, repr(exception))
                log_vrli(vrli_log_url, 
                "[{0}] Exception raised \nclient : {1} \n\n {2} \n\n Edge dict: {3} \n\n VDC Resource: {4}".format(org.get_name(), client, exception_payload, json.dumps(edge), vdc_resource), 
                {'appname':'prod:nsx_computeparser', 'level':'error'})
                continue

            #edge_details_list = []
            for edge_id in edge_ids:

                try:
                    
                    # GET a more detailed blob for this particular edge, this should include an 
                    # array of vnics
                    response           = rest_call(vrli_log_url, org.get_name(), "get", url, "/network/edges/"+edge_id, headers)
                    response_edge_dict = xmltodict.parse(response.text)
                
                    index = -1
                    for vnic in response_edge_dict['edge']['vnics']['vnic']:

                        # only grab vnics that are of type upline, and have "Internet" in their portgroup name, 
                        # and are connected
                        if vnic['type'] == "uplink" and "Public" in vnic['portgroupName'] and vnic['isConnected'] == "true":
                            index = vnic['index']
                            break
                    else:
                        continue

                    # get the statistics for the edge interfaces that are uplinks
                    response      = rest_call(vrli_log_url, org.get_name(), "get", url, "/network/edges/" + edge_id + "/statistics/interfaces/uplink", headers)
                    response_dict = xmltodict.parse(response.text)

                    # set the delta to poll for the last hour
                    date_to_poll  = int(datetime.datetime.now().timestamp())-3600

                    # for every entry in the interface statistic data
                    for entry in response_dict['statistics']['data']['statistic']:

                        # verify the index matches up with our index
                        if index == entry['vnic']:

                            # if it does, verify the timestamp is greater than the delta
                            if int(entry['timestamp']) > date_to_poll:
                                
                                # append to our list of dicitionaries
                                edge_details_list.append({
                                    "edge_id":          response_edge_dict['edge']['id'],
                                    "edgE_name":        response_edge_dict['edge']['name'],
                                    "edge_ha":          response_edge_dict['edge']['features']['highAvailability']['enabled'],
                                    "edge_size":        response_edge_dict['edge']['appliances']['applianceSize'], 
                                    "edge_status":      "0",
                                    "edge_type":        "Advanced",
                                    "edge_uplinkmbps":  entry['out'],
                                    "edge_uplinkmbpsin":entry['in'],
                                    "parent_id":        "urn:vcloud:vdc:"+response_edge_dict['edge']['datacenterMoid'],
                                    "parent_name":      response_edge_dict['edge']['datacenterName'],
                                    "last_update":      datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                })
                                
                                log_vrli(vrli_log_url, 
                                        "[{0}] edge_resource:\n\n {1}".format(org.get_name(), edge_details_list[-1]), 
                                        {'appname':'prod:nsx_computeparser', 'level':'informational'}) 

                except Exception as exception:
                    exception_payload = "\n line number {0} \n exception: ".format(sys.exc_info()[-1].tb_lineno, repr(exception))
                    log_vrli(vrli_log_url, 
                    "[{0}] Exception raised \nclient : {1} \n\n exception: {2}".format(org.get_name(), client, exception_payload), 
                    {'appname':'prod:nsx_computeparser', 'level':'error'})
                    continue
    except Exception as e:
        exception_payload = "\n line number {0} \n exception: ".format(sys.exc_info()[-1].tb_lineno, repr(exception))
        log_vrli(vrli_log_url, 
        "[{0}] Exception raised \nclient : {0} \n\n exception: {1}".format(org.get_name(), client, exception_payload), 
        {'appname':'prod:nsx_computeparser', 'level':'error'})

        # in event of an error, release our token, and close the db connection
        print(repr(exception))
        release_x_vcloud_token(vrli_log_url, url, headers)
        return
    
    release_x_vcloud_token(vrli_log_url, url, headers)

def write_interface_statistics(vrli_log_url, conn, db_table, edge_details):
    # edge_details as a list of dicitionaries, for each entry, write out to the db
    with mutex:
        cursor = conn.cursor()
        for edge_detail in edge_details:
            write_to_db(vrli_log_url, "nsx_computeparser", conn, cursor, db_table, edge_detail_list)
        cursor.close()

def terminate_connection(conn, cursor):
    if conn is not None:
        conn.close()

def ascertain_x_vcloud_token(vrli_log_url, url, username, organization, password):

    # build basic auth, and populate our header
    username             = username+"@"+organization
    authorization_string = username+":"+password
    authorization_string = base64.b64encode(authorization_string.encode('UTF-8'))

    headers = {
        'authorization':'Basic ' + str(authorization_string.decode('UTF-8')),
        'Accept'       :'application/*+xml;version=29.0',
        'Content-Type' :'application/xml'
    }

    # get our auth token
    body = {}
    headers['x-vcloud-authorization'] = rest_call(vrli_log_url, "XXXX", "post", url, "/api/sessions", headers)

    return headers

def release_x_vcloud_token(vrli_log_url, url, headers):
    rest_call(vrli_log_url, "XXXX", "delete", url, "/api/session", headers)

def meter_vcd_nsx(vcd_instance, 
                organization, 
                username, 
                password, 
                vrli_log_url,
                db_host, 
                db_name, 
                db_username, 
                db_password):
    global edge_details_list
    try:

        vcloud_log = "pyvcloud_compute_parser_" + str(datetime.datetime.now())+ ".log"
        threads    = []

        # ascertain a token
        vcloud_client    = Client(vcd_instance, 
                                    api_version='29.0', 
                                    verify_ssl_certs=False, 
                                    log_file=vcloud_log,
                                    log_requests=True, 
                                    log_headers=True, 
                                    log_bodies=True)
        vcloud_client.set_credentials(BasicLoginCredentials(username, organization, password))

        # get the list of orgs from vcd, and invoke edge_uplink_statistics
        # for each org on each new thread
        orgs = vcloud_client.get_org_list()
        for org in orgs:
            threads.append(threading.Thread(name="NSX_"+org.get('name'), 
                                            target=write_edge_uplink_statistics, 
                                            args = (vrli_log_url, 
                                                    vcloud_client, 
                                                    org, 
                                                    vcd_instance, 
                                                    db_host, 
                                                    db_name, 
                                                    db_username, 
                                                    db_password, 
                                                    organization, 
                                                    username,
                                                    password)))

            # spawn it as a daemon, i.e. it dies if the `main` thread dies
            threads[len(threads)-1].daemon = True
            threads[len(threads)-1].start()
    except Exception as exception:
        print(repr(exception))


    # join the threads back to the main thread
    for thread in threads:
        thread.join()
    
    # write the interface statistics to the DB
    conn    = initialize_connection(db_host, db_name, (db_username, db_password))
    cursor  = conn.cursor()
    try:
        write_to_db(vrli_log_url, "nsx_computeparser", conn, cursor, "vCloud_Edges", edge_details_list)
    except Exception as e:
        log_string = "Writing to DB failed with exception: \n\n{1}".format(repr(e))
        log_vrli(vrli_log_url, log_string, {'appname':'nsx_computeparser', 'level':'error'})

    if conn is not None:
        print("Connection closed")
        conn.close()
        if cursor is not None:
            print("Cursor closed")
            cursor.close()

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("url",          help="Url of the VCloud Director instance")
    parser.add_argument("organization", help="Organization for which the Service Account Belongs to")
    parser.add_argument("vcd_username", help="Username of the VCD Service Account")
    parser.add_argument("vcd_password", help="Password of the VCD Service Account")
    parser.add_argument("vrli_url",     help="Url of the VRealize log server")
    parser.add_argument("db_host",      help="Host/IPAddress of the Database")
    parser.add_argument("db_name",      help="Database for the tables to store metering data")
    parser.add_argument("db_username",  help="Username of the DB account")
    parser.add_argument("db_password",  help="Password of the DB account")
    args = parser.parse_args()

    pr = cProfile.Profile()
    pr.enable()   
    meter_vcd_nsx(args.url, 
                  args.organization, 
                  args.vcd_username, 
                  args.vcd_password,
                  args.vrli_url,
                  args.db_host,
                  args.db_name,
                  args.db_username,
                  args.db_password)
    pr.disable()
    pr.print_stats()
