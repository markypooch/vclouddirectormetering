from   pyvcloud.vcd.client              import *
from   pyvcloud.vcd.utils               import *
from   pyvcloud.vcd.org                 import *
from   pyvcloud.vcd.vdc                 import *
from   pyvcloud.vcd.system              import * 

from   metering.vcd_shared_rest_methods import *
from   metering.vcd_db                  import *
from   metering.vrli_logger             import *

import threading, requests, base64, xmltodict, xml, datetime, cProfile, argparse, json, sys
requests.packages.urllib3.disable_warnings()

storage_profile_list = []
def retrieve_org_storage_profiles(org_resource,
                                  vrli_log_url,
                                  x_vcloud_headers,
                                  client):

    global storage_profile_list
    log_vrli(vrli_log_url, 
             "Thead Instantiated, arguments are \nclient : {0} \n\norg_resource : {1}".format(client, org_resource), 
             {'appname':'storageparser', 'level':'Informational'})
    try:    

        org           = Org(client, resource=org_resource)

        # write this organization to the DB
        # get the list of vdc resources, we will pipe these into an array of the organization object, "vdc_array"
        vdc_resources     = org.list_vdcs()
        log_vrli(vrli_log_url, 
                 "[{0}] VDCs polled from org and assigned to variable: \nvdc_resources : {1}".format(org.get_name(), vdc_resources), 
                 {'appname':'storageparser', 'level':'Informational'})    

        # for each vdc_resource in our list
        for vdc_resource in vdc_resources:
            try:
                
                # append to our dicitionary the name of the vdc, and an array for the virtual machine
                vdc                       = VDC(client, resource=org.get_vdc(vdc_resource['name'])) 
                storage_profile_resources = vdc.get_storage_profiles()
            
                log_vrli(vrli_log_url, 
                        "[{0}] storage_profile_resources : {1}".format(org.get_name(), storage_profile_resources), 
                        {'appname':'storageparser', 'level':'Informational'})    

                for sp_resource in storage_profile_resources:
                    try:
                        if 'href' in sp_resource.attrib.keys():
                            storage_profile = xmltodict.parse(requests.request('GET', sp_resource.attrib['href'], 
                                                            headers=x_vcloud_headers, 
                                                            verify=False).content)
                            if 'VdcStorageProfile' in storage_profile:
                                storage_profile_list.append({
                                    'profile_id':     sp_resource.attrib['href'],
                                    'profile_name':   storage_profile['VdcStorageProfile']['@name']   if '@name'   in storage_profile['VdcStorageProfile'] else 'N/A',
                                    'profile_description': 'N/A',
                                    'profile_enabled':storage_profile['VdcStorageProfile']['Enabled'] if 'Enabled' in storage_profile['VdcStorageProfile'] else 'N/A',
                                    'profile_limit':  storage_profile['VdcStorageProfile']['Limit']   if 'Limit'   in storage_profile['VdcStorageProfile'] else 'N/A',
                                    'profile_used':   storage_profile['VdcStorageProfile']['StorageUsedMB'] if 'StorageUsedMB' in storage_profile['VdcStorageProfile'] else 'N/A',
                                    'profile_units':  storage_profile['VdcStorageProfile']['Units']   if 'Units'   in storage_profile['VdcStorageProfile'] else 'N/A',
                                    'orgVdc_name':    vdc_resource['name']                            if 'name'    in vdc_resource                         else 'N/A',
                                    'orgVdc_Id':      vdc.resource.attrib['id']                       if 'id'      in vdc.resource.attrib                  else 'N/A',
                                    'timeDate':       datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                })

                                log_vrli(vrli_log_url, 
                                        "[{0}] \n\n storage_profile entry : {1}".format(org.get_name(), storage_profile_list[-1]), 
                                        {'appname':'storageparser', 'level':'Informational'})    
                    except Exception as exception:
                        log_vrli(vrli_log_url, 
                                "[{0}] Exception in parsing storage_profile resource: \n Storage Profile Resource : {1} \n Exception: {2}".format(org.get_name(), sp_resource, repr(exception)), 
                                {'appname':'storageparser', 'level':'error'})    
            except Exception as exception:
                log_vrli(vrli_log_url, 
                        "[{0}] Failed to retrieve list of storage profile resources: \n VDC Resource: {1} \n Exception: {2}".format(org.get_name(), vdc_resource, repr(exception)), 
                        {'appname':'storageparser', 'level':'error'})    
    except Exception as exception:
        log_vrli(vrli_log_url, 
                "[{0}] Failed to retrieve list of VDC resources: \n ORG Resource: {1} \n Exception: {2}".format(org.get_name(), org_resource, repr(exception)), 
                {'appname':'storageparser', 'level':'error'})    
    return

def main(url, organization, username, password, vrli_url, db_host, db_name, db_username, db_password):
    
    try:

        x_vcloud_headers = ascertain_x_vcloud_token(vrli_url, url, username, organization, password)
        vcloud_log = "pyvcloud_storage_parser_" + str(datetime.datetime.now())+ ".log"

        # ascertain a token
        vcloud_client    = Client(url, 
                                    api_version='29.0', 
                                    verify_ssl_certs=False, 
                                    log_file=vcloud_log,
                                    log_requests=True, 
                                    log_headers=True, 
                                    log_bodies=True)
        vcloud_client.set_credentials(BasicLoginCredentials(username, organization, password))

        # get the list of orgs from vcd, and invoke edge_uplink_statistics
        # for each org on each new thread
        orgs    = vcloud_client.get_org_list()
        threads = []
        for org in orgs:
            threads.append(threading.Thread(name="Storage_"+org.get('name'), 
                                            target=retrieve_org_storage_profiles, 
                                            args = (org, vrli_url, x_vcloud_headers, vcloud_client)))
            
            # spawn it as a daemon, i.e. it dies if the `main` thread dies
            threads[len(threads)-1].daemon = True
            threads[len(threads)-1].start()

        # join the threads back to the main thread
        for thread in threads:
            thread.join()

        # write the interface statistics to the DB
        conn    = initialize_connection(db_host, db_name, (db_username, db_password))
        cursor  = conn.cursor()
        try:
            write_to_db(vrli_url, "storageparser", conn, cursor, "vCloud_Vdc_Storage", storage_profile_list)
        except Exception as exception:
            log_string = "Writing to DB failed with exception: \n\n{1}".format(repr(exception))
            log_vrli(vrli_url, log_string, {'appname':'storageparser', 'level':'error'})

        if conn is not None:
            print("Connection closed")
            conn.close()
            if cursor is not None:
                print("Cursor closed")
                cursor.close()

    except Exception as exception:
        print(repr(exception))
        log_vrli(vrli_url, "Exception raised:\n {0}".format(repr(exception)), {'appname':'storageparser', 'level':'error'})

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("url",          help="Url of the VCloud Director instance")
    parser.add_argument("organization", help="Organization for which the Service Account Belongs to")
    parser.add_argument("vcd_username", help="Username of the VCD Service Account")
    parser.add_argument("vcd_password", help="Password of the VCD Service Account")
    parser.add_argument("vrli_url",     help="Url of the VRealize log server")
    parser.add_argument("db_host",      help="Host/IPAddress of the Database")
    parser.add_argument("db_name",      help="Database for the tables to store metering data")
    parser.add_argument("db_username",  help="Username of the DB account")
    parser.add_argument("db_password",  help="Password of the DB account")
    args = parser.parse_args()

    pr = cProfile.Profile()
    pr.enable()   
    main(args.url, 
         args.organization, 
         args.vcd_username, 
         args.vcd_password,
         args.vrli_url,
         args.db_host,
         args.db_name,
         args.db_username,
         args.db_password)
    pr.disable()
    pr.print_stats()
