from   metering.vrli_logger import *
import requests, base64

# method to encapuslate restful calls made in the script
def rest_call(vrli_log_url, org_name, method, url, endpoint, headers, appname="nsx_computeparser", body=None):
    request_url = url + endpoint
    response    = None

    # set the level for logging relative to the status_code
    response = requests.request(method=method, url=request_url,  headers=headers, verify=False)
    level    = "error" if response.status_code < 200 or response.status_code >= 300 else "Informational"

    # log to vrli either successful, or error
    response_text = response.text
    if len(response_text) > 500:
        response_text = response_text[:500]
    log_vrli(vrli_log_url, 
            "ReSTFul message:\n \nOrg: [{0}] url: {1} \nmethod: {2} \nstatus: {3} \n message: {4}".format(org_name,
                                                                                                          request_url,
                                                                                                          method,
                                                                                                          str(response.status_code),
                                                                                                          response_text), 
            {'appname':appname, 'level':level})
    if 'api/sessions' in request_url and (response.status_code >= 200 and response.status_code < 300):
        response = response.headers['x-vcloud-authorization']
    return response

def ascertain_x_vcloud_token(vrli_log_url, url, username, organization, password):

    # build basic auth, and populate our header
    username             = username+"@"+organization
    authorization_string = username+":"+password
    authorization_string = base64.b64encode(authorization_string.encode('UTF-8'))

    headers = {
        'authorization':'Basic ' + str(authorization_string.decode('UTF-8')),
        'Accept'       :'application/*+xml;version=29.0',
        'Content-Type' :'application/xml'
    }

    # get our auth token
    body = {}
    headers['x-vcloud-authorization'] = rest_call(vrli_log_url, "XXXX", "post", url, "/api/sessions", headers)

    return headers