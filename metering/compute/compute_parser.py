from   pyvcloud.vcd.client     import *
from   pyvcloud.vcd.org        import *
from   pyvcloud.vcd.vapp       import *
from   pyvcloud.vcd.vm         import *
from   pyvcloud.vcd.utils      import *

# import 
from   metering.vcd_db         import *
from   metering.vrli_logger    import *

import threading, unittest, datetime, requests, logging, cProfile, json, argparse
requests.packages.urllib3.disable_warnings()

mutex = threading.Lock()

# a class (more like structure in this context) to hold our organizations
class Organization:
    def __init__(self):
        self.vdc_array            = []
        self.virtual_applications = []
        self.virtual_machine      = []
        self.org_dict = {
            'org_name'         : "",
            'org_id'           : "",
            'org_description'  : "",
            'org_full_name'    : "",
            'org_is_enabled'   : "",
            'vcloud_host_id'   : "",
            'vcloud_host_name' : "",
            'vcloud_host_url'  : "",
            'last_update'      : datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }

organization_list = []
def meter_organization(client, org_resource, vrli_log_url, vcd_username, vcd_password, db_host, db_name, db_username, db_password):

    global organization_list
    log_vrli(vrli_log_url, 
             "Thead Instantiated, arguments are \nclient : {0} \n\norg_resource : {1}".format(client, org_resource), 
             {'appname':'prod:computeparser', 'level':'Informational'})
    try:    

        org           = Org(client, resource=org_resource)

        # an important lock, without this, the race condition could exist that two threads append to the shared organizations
        # list and the index could refer to the incorrect organization
        organizations = Organization()

        organizations.org_dict['org_name']        = org.get_name()
        organizations.org_dict['org_id']          = org.resource.attrib['id']
        organizations.org_dict['org_description'] = org.resource.Description
        organizations.org_dict['org_full_name']   = org.resource.FullName
        organizations.org_dict['org_is_enabled']  = org.update_org()['IsEnabled']

        organization_index = 0
        with mutex:
            organization_list.append(organizations)
            organization_index = len(organization_list)-1
            
        # log the org id polled from VDC
        log_vrli(vrli_log_url, 
                 "[{0}] OrgID Polled : {1}".format(org.get_name(),organization_list[organization_index].org_dict['org_id']), 
                 {'appname':'prod:computeparser', 'level':'Informational'})    

        # write this organization to the DB
        # get the list of vdc resources, we will pipe these into an array of the organization object, "vdc_array"
        vdc_resources     = org.list_vdcs()
        log_vrli(vrli_log_url, 
                 "[{0}] VDCs polled from org and assigned to variable: \nvdc_resources : {1}".format(org.get_name(), vdc_resources), 
                 {'appname':'prod:computeparser', 'level':'Informational'})    

        # for each vdc_resource in our list
        for vdc_resource in vdc_resources:

            try:
                # append to our dicitionary the name of the vdc, and an array for the virtual machine
                vdc              = VDC(client, resource=org.get_vdc(vdc_resource['name']))

                # cache this for readability purposes when testing for attributes
                compute_capacity = vdc.resource.ComputeCapacity

                # Unfortunately our DB column names don't match up with the class whose attributes we are polling
                # else we could have used `vdc.__dict__.iteritems():` and just looped over these attributes
                organization_list[organization_index].vdc_array.append({
                    'vdc_id':               vdc.resource.attrib['id']              if 'id' in vdc.resource.attrib.keys()            else "N/A",
                    'vdc_name':             vdc.name                               if hasattr(vdc, "name")                          else "N/A", 
                    'vdc_is_enabled':       vdc.resource.IsEnabled                 if hasattr(vdc.resource, "IsEnabled")            else "N/A",
                    'vdc_cpu_alloc':        compute_capacity.Cpu.Allocated         if hasattr(compute_capacity.Cpu, "Allocated")    else "N/A",
                    'vdc_cpu_limit':        compute_capacity.Cpu.Limit             if hasattr(compute_capacity.Cpu, "Limit")        else "N/A",
                    'vdc_cpu_overhead':     compute_capacity.Cpu.Overhead          if hasattr(compute_capacity.Cpu, "Overhead")     else "N/A",
                    'vdc_cpu_reserved':     compute_capacity.Cpu.Reserved          if hasattr(compute_capacity.Cpu, "Reserved")     else "N/A",
                    'vdc_cpu_used':         compute_capacity.Cpu.Used              if hasattr(compute_capacity.Cpu, "Used")         else "N/A",
                    'vdc_cpu_units':        compute_capacity.Cpu.Units             if hasattr(compute_capacity.Cpu, "Units")        else "N/A",
                    'vdc_mhz_to_vcpu':      vdc.resource.VCpuInMhz2                if hasattr(vdc.resource, "VCpuInMhz2")           else "N/A",
                    'vdc_memory_alloc':     compute_capacity.Memory.Allocated      if hasattr(compute_capacity.Memory, "Allocated") else "N/A",
                    'vdc_memory_limit':     compute_capacity.Memory.Limit          if hasattr(compute_capacity.Memory, "Limit")     else "N/A",
                    'vdc_memory_overhead':  compute_capacity.Memory.Overhead       if hasattr(compute_capacity.Memory, "Overhead")  else "N/A",
                    'vdc_memory_used':      compute_capacity.Memory.Used           if hasattr(compute_capacity.Memory, "Used")      else "N/A",
                    'vdc_memory_units':     compute_capacity.Memory.Units          if hasattr(compute_capacity.Memory, "Units")     else "N/A",
                    'vdc_memory_reserved':  compute_capacity.Memory.Reserved       if hasattr(compute_capacity.Memory, "Reserved")  else "N/A",
                    'vdc_used_net_cnt':     vdc.resource.UsedNetworkCount          if hasattr(vdc.resource, "UsedNetworkCount")     else "N/A",
                    'vdc_vm_quota':         vdc.resource.VmQuota                   if hasattr(vdc.resource, "VmQuota")              else "N/A",
                    'vdc_allocation_model': vdc.resource.AllocationModel           if hasattr(vdc.resource, "AllocationModel")      else "N/A",
                    'vdc_description':      vdc.resource.Description               if hasattr(vdc.resource, "Description")          else "N/A",
                    'parent_id':            organization_list[organization_index].org_dict['org_id'],  
                    'parent_name':          organization_list[organization_index].org_dict['org_name'],
                    'last_update':          datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                })
            except Exception as exception:
                log_vrli(vrli_log_url, 
                         "[{0}] Failed to poll VDC[{1}] \n\nexception raised :\n{2}".format(org.get_name(), vdc_resource['name'], repr(exception)), 
                         {'appname':'prod:computeparser', 'level':'error'})
                continue

            # list all vapp resources in the vdc
            vapp_resources = vdc.list_resources(EntityType.VAPP)
            log_vrli(vrli_log_url, 
                     "[{0}] Vapps polled from VDC[{1}] \nvapp_resources : {2}".format(org.get_name(), vdc.name, vapp_resources), 
                     {'appname':'prod:computeparser', 'level':'Informational'})    

            for vapp_resource in vapp_resources:
                
                try:
                    # retrieve the 'href', and pipe it into our array
                    vapp = VApp(client, resource=vdc.get_vapp(vapp_resource['name']))

                    if 'status'   in vapp.resource.attrib.keys():

                        # https://www.vmware.com/support/vcd/doc/rest-api-doc-1.5-html/operations/GET-VApp.html
                        # https://docs.onapp.com/vcd/latest/api-guide/vapps-api/get-vapp-details
                        if vapp.resource.attrib['status'] in ('1', '2', '3', '4', '5', '8', '10'):
                            try:
                                # grab the vapp details         
                                organization_list[organization_index].virtual_applications.append({
                                    'vapp_id':            vapp.resource.attrib['id']              if 'id'       in vapp.resource.attrib.keys()   else "N/A",
                                    'vapp_name':          vapp.resource.attrib['name']            if 'name'     in vapp.resource.attrib.keys()   else "N/A",
                                    'vapp_deployed':      vapp.resource.attrib['deployed']        if 'deployed' in vapp.resource.attrib.keys()   else "N/A",
                                    'vapp_status':        vapp.resource.attrib['status']          if 'status'   in vapp.resource.attrib.keys()   else "N/A", # convert to human readable value
                                    'vapp_description':   vapp.resource.Description               if hasattr(vapp.resource, "Description")       else "N/A",
                                    'vapp_in_maintenance':vapp.resource.InMaintenanceMode         if hasattr(vapp.resource, "InMaintenanceMode") else "N/A",
                                    'vapp_owner':         vapp.resource.Owner.User                if hasattr(vapp.resource.Owner, "User")        else "N/A",
                                    'vapp_fullStatus':    vapp.resource.attrib['status']          if 'status'   in vapp.resource.attrib.keys()   else "N/A", # convert to human readable value
                                    'parent_name':        vdc.name                                if hasattr(vdc, "name")                        else "N/A",
                                    'parent_id':          vdc.resource.attrib['id'],
                                    'last_update':        datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                })
                            except Exception as exception:
                                log_vrli(vrli_log_url, 
                                        "[{0}] Failed to poll VApp[{1}] \n\nexception raised :\n{2}".format(org.get_name(), vapp_resource['name'], repr(exception)), 
                                        {'appname':'prod:computeparser', 'level':'error'})
                                continue

                            # retrieve all vm_resources from the vapp
                            vm_resources = vapp.get_all_vms()
                            for vm_resource in vm_resources:

                                try:
                                    # cached for readability purposes
                                    vm_spec_section = vm_resource.VmSpecSection

                                    organization_list[organization_index].virtual_machine.append({
                                        'vm_id'  :           vm_resource.attrib['id']                     if 'id'       in vm_resource.attrib.keys()                 else "N/A", 
                                        'vm_name':           vm_resource.attrib['name']                   if 'name'     in vm_resource.attrib.keys()                 else "N/A", 
                                        'vm_deployed':       vm_resource.attrib['deployed']               if 'deployed' in vm_resource.attrib.keys()                 else "N/A", 
                                        'vm_status':         vm_resource.attrib['status']                 if 'status'   in vm_resource.attrib.keys()                 else "N/A", 
                                        'vm_description':    vm_resource.Description                      if hasattr(vm_resource, "Description")                     else "N/A",
                                        'vm_vcpu':           vm_spec_section.NumCpus                      if hasattr(vm_spec_section, "NumCpus")                     else "N/A",
                                        'vm_cores_socket':   vm_spec_section.NumCoresPerSocket            if hasattr(vm_spec_section, "NumCoresPerSocket")           else "N/A",
                                        'vm_memory':         vm_spec_section.MemoryResourceMb.Configured  if hasattr(vm_spec_section.MemoryResourceMb, "Configured") else "N/A",
                                        'vm_os':             vm_spec_section.OsType                       if hasattr(vm_spec_section, "OsType")                      else "N/A",
                                        'parent_id':         vapp.resource.attrib['id'],
                                        'parent_name':       vapp.resource.attrib['name'],
                                        'last_update':       datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                    })

                                    log_vrli(vrli_log_url, 
                                            "[{0}] VM polled from Vapp[{1}] in VDC[{2}] \nvm_resource :\n\n {3}".format(org.get_name(), vapp.resource.attrib['name'], vdc.name, organization_list[organization_index].virtual_machine[-1]), 
                                            {'appname':'prod:computeparser', 'level':'Informational'})
                                except Exception as exception:

                                    # log error
                                    log_vrli(vrli_log_url, 
                                            "[{0}] Failed to poll VM[{1}] \n\nexception raised :\n{2}".format(org.get_name(), vm_resource.attrib['name'], repr(exception)), 
                                            {'appname':'prod:computeparser', 'level':'error'})
                                    continue
                        else:
                            # log error
                            log_vrli(vrli_log_url, 
                                    "[{0}] VApp in non-pollable state [{1}]\n\nstate :\n{2}".format(org.get_name(), vapp.resource.attrib['name'], repr(vapp.resource.attrib['status'])), 
                                    {'appname':'prod:computeparser', 'level':'error'})
                            continue
                except Exception as exception:
                    log_vrli(vrli_log_url, 
                            "[{0}] Failed to poll VApp[{1}] \n\nexception raised :\n{2}".format(org.get_name(), vapp_resource['name'], repr(exception)), 
                            {'appname':'prod:computeparser', 'level':'error'})
                    continue

    except Exception as exception:
        log_vrli(vrli_log_url, 
                 "[{0}] \n\nexception raised :\n {1}".format(org.get_name(), repr(exception)), 
                 {'appname':'prod:computeparser', 'level':'error'})

def meter_vcd_compute(vcd_instance, 
                      organization, 
                      username, 
                      password, 
                      vrli_log_url,
                      db_host, 
                      db_name, 
                      db_username, 
                      db_password):

    #create a log name based upon current datetime
    vcloud_log = "pyvcloud_compute_parser_" + str(datetime.datetime.now())+ ".log"
    threads    = []

    # ascertain a token
    vcloud_client    = Client(vcd_instance, 
                                api_version='29.0', 
                                verify_ssl_certs=False, 
                                log_file=vcloud_log,
                                log_requests=True, 
                                log_headers=True, 
                                log_bodies=True)
    vcloud_client.set_credentials(BasicLoginCredentials(username, organization, password))
    
    # get the list of orgs from vcd
    # for each org, spawn a thread passing the respective org, vcloud_client handle, and a reference
    # to our `test_vcd_get_org_data` method.
    orgs = vcloud_client.get_org_list()
    for org in orgs:
        threads.append(threading.Thread(name=org.get('name'), 
                                        target=meter_organization, 
                                        args = (vcloud_client, 
                                                org,  
                                                vrli_log_url, 
                                                username, 
                                                password, 
                                                db_host, 
                                                db_name, 
                                                db_username, 
                                                db_password)))

        # spawn it as a daemon, i.e. it dies if the `main` thread dies
        threads[len(threads)-1].daemon = True
        threads[len(threads)-1].start()

    # join the threads back to the main thread
    for thread in threads:
        thread.join()

    conn, cursor = None, None
    try:
        conn   = initialize_connection(db_host, db_name, (db_username, db_password))
        cursor = conn.cursor() 
        for organization in organization_list:
            write_to_db(vrli_log_url, "computeparser", conn, cursor, 'vCloud_Orgs',  organization.org_dict)
            write_to_db(vrli_log_url, "computeparser", conn, cursor, 'vCloud_Vdcs',  organization.vdc_array)
            write_to_db(vrli_log_url, "computeparser", conn, cursor, 'vCloud_Vms',   organization.virtual_machine)
            write_to_db(vrli_log_url, "computeparser", conn, cursor, 'vCloud_Vapps', organization.virtual_applications)
    except Exception as e:
        log_string = "Writing to DB failed with exception: \n\n{0}".format(repr(e))
        log_vrli(vrli_log_url, log_string, {'appname':'computeparser', 'level':'error'})

    if conn   is not None:
        print("Connection closed")
        conn.close()
        if cursor is not None:
            print("Cursor closed")
            cursor.close()

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("url",          help="Url of the VCloud Director instance")
    parser.add_argument("organization", help="Organization for which the Service Account Belongs to")
    parser.add_argument("vcd_username", help="Username of the VCD Service Account")
    parser.add_argument("vcd_password", help="Password of the VCD Service Account")
    parser.add_argument("vrli_url",     help="Url of the VRealize log server")
    parser.add_argument("db_host",      help="Host/IPAddress of the Database")
    parser.add_argument("db_name",      help="Database for the tables to store metering data")
    parser.add_argument("db_username",  help="Username of the DB account")
    parser.add_argument("db_password",  help="Password of the DB account")
    args = parser.parse_args()

    pr = cProfile.Profile()
    pr.enable()
    meter_vcd_compute(args.url, 
                      args.organization, 
                      args.vcd_username, 
                      args.vcd_password,
                      args.vrli_url,
                      args.db_host,
                      args.db_name,
                      args.db_username,
                      args.db_password)
                      
    pr.disable()
    pr.print_stats(sort='time')
