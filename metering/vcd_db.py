from   metering.vrli_logger import *
import mysql.connector

def initialize_connection(db_host, db_name, credentials):
    conn = None
    conn = mysql.connector.connect(host=db_host,
                                   database=db_name,
                                   user=credentials[0],
                                   password=credentials[1], 
                                   connection_timeout=10)
    if conn is None:
        raise Exception("Error: Connection to DB return None")
    return conn

def write_to_db(vrli_log_url, app_name, conn, cursor, db_table, dicitionary_list):
    
    try:
        if not isinstance(dicitionary_list, list):
            dicitionary_list = [dicitionary_list]
        if len(dicitionary_list) <= 0:
            return

        # build our query string (will contain the column names/keys of the dictionary), and value string
        # will contain the values we wish to write to the record.
        query_string         = "INSERT IGNORE INTO " + db_table + " ("
        value_string         = " VALUES ("

        # convert the iterators into list types
        dicitionary_list_keys   = list(dicitionary_list[0].keys())

        # loop through our values, and append to the value string
        for i in range(len(dicitionary_list_keys)):
            value_string += "%s"
            query_string += dicitionary_list_keys[i]
            if i + 1 < len(dicitionary_list_keys):
                value_string += ", "
                query_string += ", "
        query_string += ") "
        value_string += ")"

        rows = []
        for dicitionary in dicitionary_list:
            row = []
            for k,v in dicitionary.items():
                row.append(str(v))
            rows.append(row)

        # concatenate the strings together to form our query
        query_string += value_string
        log_string = "Query String :\n{0} Values: \n\n {1}".format(query_string, rows)
        #print(log_string)
        #log our query
        log_vrli(vrli_log_url, log_string, {'appname':app_name, 'level':'Informational'})
        try:
            cursor.executemany(query_string, rows)
            conn.commit()
            print("Success!")
        except Exception as exception:
            # log error, failed to write to DB
            # save json body locally, and push to repo for recovering of data
            log_string = "Query String :\n{0} Failed with exception: \n\n{1}".format(query_string, repr(exception))
            log_vrli(vrli_log_url, log_string, {'appname':app_name, 'level':'error'})
            print(repr(exception))
            pass
    except Exception as exception:
        log_string = "DB Method Failed with exception: \n\n{1}".format(repr(exception))
        log_vrli(vrli_log_url, log_string, {'appname':app_name, 'level':'error'})
