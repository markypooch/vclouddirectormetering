import requests

def log_vrli(url, text, dictionary):

    if not isinstance(url, str):
        raise ValueError("url is required to be of instance, 'str'")
    if not isinstance(text, str):
        raise ValueError("text is required to be of instance, 'str'")
    if not isinstance(dictionary, dict):
        raise ValueError("dicitionary is required to be of instance, 'dict'")

    body                     = {}
    body['events']           = []
    body['events'].append({'text':text, 'fields':[]})
    for key, value in dictionary.items():
        body['events'][len(body['events'])-1]['fields'].append({"name":key, "content":value})

    headers = {"Content-type":"application/json"}
    result  = requests.post('https://'+url+"/api/v1/events/ingest/127.0.0.1", headers=headers, json=body, verify=False)
    if result.status_code < 200 or result.status_code >= 300:
        print(result.status_code)
        print(result.text)
    